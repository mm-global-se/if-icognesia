mmcore.IntegrationFactory.register('iCognesia', {
    validate: function (data) {
        if(!data.campaign)
            return 'No campaign.';

        return true;
    },

    check: function (data) {
        return window.iCognesia && typeof window.iCognesia.RegCustomVars === 'function';
    },

    exec: function (data) {
        var mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
            campaignInfo = data.campaignInfo.replace('=', '::'),
            customVarValue = 'maxytest=' + mode + campaignInfo;
      
        iCognesia.RegCustomVars([customVarValue]);

        if (typeof data.callback === 'function') data.callback.call(null, data.campaignInfo);
        return true;
    }
});