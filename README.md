# iCognesia

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to __iCongnesia__.

## How we send the data

We are calling `window.iCognesia.RegCustomVars` method with campaign experience information (see data format [below](#data-format)) as an argument (wrapped in array).

_Example_:

```javascript
iCognesia.RegCustomVars(['maxytest=MM_Prod_T1Button::color:red']);

```

## Data Format

The data sent to iCognesia will be in the following format:

`maxytest=<mode>_<campaign name>::<element1>:<variant>|<element2>:<variant>`

## Download

* [icognesia-register.js](https://bitbucket.org/mm-global-se/if-icognesia/src/master/src/icognesia-register.js)

* [icognesia-initialize.js](https://bitbucket.org/mm-global-se/if-icognesia/src/master/src/icognesia-initialize.js)

## Prerequisite

The following information needs to be provided by the client: 

+ Campaign Name

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [icognesia-register.js](https://bitbucket.org/mm-global-se/if-icognesia/src/master/src/icognesia-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [icognesia-initialize.js](https://bitbucket.org/mm-global-se/if-icognesia/src/master/src/icognesia-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.## Deployment instructions

### QA

+ Open Chrome DevTools

+ Go to Network tab

+ Filter by `.intelli-direct.com`

+ Click on 'Header' tab

+ Check the `pdata` parameter - it must contain campaign ifromation

![qa](assets/cognesia.png)